# Tuition-BackboneJS

Easy introduction into Javascript MVC with BackboneJS and MarionetteJS

Step 1: Install required stuff:

* install node js: https://nodejs.org/en/download/
* install nvm: curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
* install backbone js, marionette.js

Step 2: First File - Hello World

* create a new file and name it index.html
* for now we will include the file directly
* create a script tag and include the backbone cdn 
* backbonejs require underscore js as a dependant
* so create another script tag and include the underscore cdn

Step 3: Let's create our first Model and View
